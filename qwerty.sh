#!/bin/bash


# Переменные
SOURCE=/dev/sda1
RESTOREDISK=`lsblk -flp | grep DATA | awk '{print $1}'` #ищем среди дисков файловую систему с тегом "DATA"
DESTINATION=`mount ${RESTOREDISK} /mnt`


# Функция создания образа с псевдографикой
function f_backup {
        (pv -n ${SOURCE} | gzip -1 > ${DESTINATION}/sdatest.ddimg.gz ) 2>&1 | whiptail --title "SYSTEM RESTORING" --gauge "PLEASE WAIT..." 10 70 0
}


# Подтверждение завершения бекапа
function f_ok {
        whiptail --title "SYSTEM BACKUP" --msgbox "BACKUP COMPLETE. PRESS OK" 10 70
}








whiptail --clear --title "START BACKUP?" --yesno "`lsblk -s`" 20 80
case $? in
        0)
                f_backup
                f_ok
                ;;

esac

whiptail --title "COMPLETE" --msgbox "TO EXIT press OK" 10 70


